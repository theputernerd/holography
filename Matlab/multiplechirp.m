%function []=multiplechirp() % put this in if you want to compile
%multiplechirp.m
%J.West
%Mar 2004
%This script is able to calculate the holographic interference fringe pattern for 
%any object described with [chirpx,chirpy,chirpz]
%The contrast is automatically taken care of with the imshow function
%
clear all;
hold off;
%%%%%%%%%%%%%the location of the chirps
%chirpx=[0 1 2 3];
%chirpy=[0 1 2 3] ;
%chirpz=[.0025 .0028 .0029 .0030];% z is the distance
objacc=1680 % this is how many chirps you want in the object  Expect to finish at 0430
chirpx=linspace(30,20,objacc);
chirpy=linspace(30,20,objacc);
chirpz=linspace(.05,.01,objacc);


[chirpx chirpy chirpz]=sphere(30);
chirpx=chirpx+25;
chirpy=chirpy+25;
%mindist=min(chirpz)^2; %this is so the closest point has a magnitude of 1.

lambda=(632.8e-9);
delta=10e-6;

screenxres=700; %the number of points between min and max
screenyres=700;
screenymin=-20;  %the size of the viewing area
screenymax=20;
screenxmin=-20;
screenxmax=20;

chirpz=(chirpz-min(chirpz(:)-.1))/abs(max(chirpz(:)))*(.5);
a=0;
alpha=0;
m=1;
xaxis=linspace(screenxmin,screenxmax,screenxres);
yaxis=linspace(screenymin,screenymax,screenyres);
d=chirpz;
t=pi*delta^2/(lambda);
t2=pi*delta^2/(400e-9);
t3=pi*delta^2/(500e-9);
prev=0;
tic
for(cc=1:length(chirpx))
 for(xx=1:screenxres)
  for(yy=1:screenyres)
      if(chirpz<.1) chirpz=0;
      end 
    E(xx,yy)=0;
      alpha=t*1/chirpz(cc);
      alpha2=t2*1/chirpz(cc);
      alpha3=t3*1/chirpz(cc);
      
      E(xx,yy)=exp(i*alpha*((xaxis(xx)+chirpx(cc)).^2+(yaxis(yy)+chirpy(cc)).^2));%+exp(i*alpha2*((xaxis(xx)+chirpx(cc)).^2+(yaxis(yy)+chirpy(cc)).^2))+exp(i*alpha3*((xaxis(xx)+chirpx(cc)).^2+(yaxis(yy)+chirpy(cc)).^2)));
end
end
prev=E+prev;
%imshow(real(prev),[]); %put this line in if you want to see the pattern being built
end
time=toc  
imshow(real(prev),[]); 
%results
%16.5 secperchirp - laptop
%11.8 secperchirp - desktop