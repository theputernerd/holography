%single chirp
clear;
hold off;
chirpx=0;
chirpy=0;
chirpz=1;%the location of the chirp
lambda=(632.8e-9);
delta=10e-6;

screenxres=400; %the number of points between min and max
screenyres=400;
screenymin=-20;  %the size of the screen
screenymax=20;
screenxmin=-20;
screenxmax=20;
a=0;
alpha=0;
m=1;
xaxis=linspace(screenxmin,screenxmax,screenxres);
yaxis=linspace(screenymin,screenymax,screenyres);
for(xx=1:screenxres)
for(yy=1:screenyres)
%    ((xaxis(xx))^2+(yaxis(yy))^2)*i*pi*(delta^2)/((lambda*yaxis(yy))*(5)));
    d=sqrt((xaxis(xx)^2+yaxis(yy)^2));
    dv(xx,yy)=d;
    alpha=pi*delta^2/(lambda*.0032);
    E1(xx,yy)=(exp(j*alpha/chirpz*((xaxis(xx))^2+(yaxis(yy))^2)));
    E2(xx,yy)=(exp(j*alpha/.5*((xaxis(xx))^2+(yaxis(yy))^2)));
    
    a=max(d,a);    
end
end
figure(1);
imshow(real(E1),[]);
title('z=1');
figure(2);
imshow(real(E2),[]);
title('z=.5');
figure(3);
ereal=(abs(real(E1-E2)));
imshow(ereal,[]);
title('unscaled difference');

%Now that its all set up time to find the scale factor that makes error close to zero
%what I will do is increment by 1 until error gets larger, then I'll increment by .1 etc
av=1000; %the average error across the image
e2real=real(E2);
inc=.1;  %starting increment
sc=double(1.3); %starting scale value
for(ctr=1:500)
 e2large=imresize(e2real,sc,'bicubic');
 beg=double(max(size(e2large(1,:)))/2-screenxres/2);
 fin=double(screenxres-1);
 e2final=imcrop(e2large,[beg beg fin fin]);
 err=(abs(real(E1)-e2final));
 tav=mean(err(:));
 if(tav==av)
    %then the resolution is not good enough to distinguis between two points
    inc=inc*2 ;  
 elseif(tav<av)
    %then we have a smaller value
    av=tav;
    bestsc=sc;
 else
  %then we've started getting larger
  %undo last move
  %and decrease the increment
  sc=bestsc;
  inc=inc*.1;
  
 end
 sc=sc+inc;
end
figure(4);
imshow(e2final,[]);
title('final scaled version');
figure(5)
%e2final(1,1)=2;
err=(abs(real(E1)-e2final));
av=mean(err(:))
err(1,1)=2; %this is to have a visual reference for the maximum error
err(1,2)=2; %this is to have a reference for the maximum error
err(2,1)=2; %this is to have a reference for the maximum error
err(2,2)=2; %this is to have a reference for the maximum error
imshow(err,[])
title('ABS Error plot')
bestsc
av

av/2*100
%RESULTS

%ScreenRES=1000
%av=0.05858376317118
%bestsc =1.41303161600000

