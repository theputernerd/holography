# README #

This is the source code and application for a computer generated holographic interference patterns. Figure out your laser wavelength, set up a system where your laser passes through these interference patterns and a hologram will appear. 


The four folders represent 4 variations in methods of chirp calculations (NB a chirp is a interference pattern which generates a single point in 3d space). 

/***********************hologlRealCalcs - 
**
*- the system calculates the interference patterns on the fly
*This version does only real calculations.
*It includes an attempt at parralax that needs to be visually confirmed.
*
*-
*/

/***********************hologlepth - 
**
*the system attempts to use pre-calculated chirps then manipulate them with transforms via positioning in a 3d space
*There is no java here. 
*To run open hologldepth/Release/plibapp.exe
*
*
*/

/***********************hologlV3Complex - Uses the complex value of the chirp 
**
*
*This version is the complete works.
*It uses complex calculations and an 800*600 complex array as the main data
*The calculations are done for each position in the array (i.e. screen)
*Time taken for 1 chirp  is 420ms on laptop P4 2.8Ghz 512Mhz

*Advantage the resolution is excellent, any artifacts appear are because of chirp properties.
*Disadvantage -- too slow.
****/



/***********************fastchirpcircledrawreal - 
**
*This version of chirp calc calculates the chirp by using a circle drawing
*algorithm.
*Weakness.. Areas that are not overwritten persist.
*there are some artifacts where some pixels are not written to
*Advantage it is fast, takes about 50ms on a p4 2.8G laptop with 512M RAM.
*The calculator uses real values only.
*/
-----------------------------

 master.java is where you specify the points you want to display in the 3D space. It calls displaychirp_addchirp. This has the main class you will need the classpath to run. 

 displaychirp.cpp is responsible for drawing the chirp to the screen. the function Java_displaychirp_addchirp is called from master.java and turns the points you want displayed into an interference pattern.

 chirpcalc.cpp is where the chirp is calculated.