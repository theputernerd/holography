 /** This class provides the interface between java and the  c++ chirp calculator
 * It is static to start with, but might be better served instantiating it.
 NOTE java doesn't touch the object it only passes it around.
 so java never needs to know what kind it is, the object truely
 is std::complex<jdouble>*
 */
class chirpcalc
{
	//public static native Object calcchirp(double x, double y, double z);
	public static native byte[] calcchirp(double x, double y, double z);
	static
	{
		System.loadLibrary("chirpcalc");
	}

}