import java.lang.Math.*;
/**master.java
  * Project holoGLjniComplex
  * Written by J.West
  * May 04
  * 
  * This class is the java executable for the project holoGLjniComplex.
  * In this class the object's points are defined. 
  * Required libraries are displaychirp.dll, chirpcalc.dll glut32.dll
  * 
  * To customise this program the following procedure should be used:
  *initialise display and start opengl in its own java thread.
  *displaychirp.newscene(); to clear the scene
  *Create chirp instances, and calculate points.
  *Pass the return directly to addchirp.
  *call d.display() to move the new array to the framebuffer
  */
public class master
{	static byte[] singlechirp;
	static double k;
	
	public static void main(String args[])
	{   //I wonder if I start this in a thread and playwith the chirps will it change the display
		
		
  long chirpcalct=System.currentTimeMillis();
		byte[] chirp2;
		singlechirp=chirpcalc.calcchirp(0,-200,.5);
		chirpcalct=System.currentTimeMillis()-chirpcalct;
		
		//System.out.println("value "+singlechirp);
		chirp2=chirpcalc.calcchirp(0,200,.5); //just do this to time it.
		System.out.println("1 Chirp calc time (ms):"+chirpcalct);
		
           new Thread() {
    		public void run() 
    		{   displaychirp.initialise();
				displaychirp.startglut();
		   	
        	    	 
    		}
  			}.start();
  		displaychirp d=new displaychirp();	
		//dispt=System.currentTimeMillis()-dispt;
		System.out.println("outside thread");
	
	//HERE is where you can create the object.
	//Create as many points as you like with d.addchirp then call d.display to 
	//tell displaychirp to update the screen
	//If addchirp was thread safe you could create as many instances as there 
	//are points, this would make it easy
	//to run on Beuwoulf or a distributed network.
	
		//	for( k=0;k<8*Math.PI;k=k+.05)
			{   displaychirp.newscene();
			// 	d.addchirp(chirpcalc.calcchirp(300*Math.sin(k)-100,200*Math.cos(k)+50,1));
			//	d.addchirp(chirpcalc.calcchirp(0,200*Math.sin(k)+100,2.3+2*Math.cos(k)));
				//d.addchirp(chirpcalc.calcchirp(300*Math.cos(k)-100,100,2.3+2*Math.sin(k)));
					d.addchirp(chirpcalc.calcchirp(0,0,1));
			 				
			 	/*new Thread(new Runnable() 
			 	 {
    				public void run() 
    				{ displaychirp.addchirp(chirpcalc.calcchirp(300*Math.sin(k)-100,200*Math.cos(k)+50,1));
					  displaychirp.display();
					}
				}).start();
				*/
				
				//try{Thread.sleep(1000);
				//}
				//catch(Exception e)
				//{
				//}
				
				
			/*	new Thread(new Runnable() 
			 	 {public void run() 
    				{displaychirp.addchirp(chirpcalc.calcchirp(0,200*Math.sin(k)+100,2.3+2*Math.cos(k)));
					}
					}).start();
				new Thread(new Runnable() 
			 	 {public void run() 
    				{displaychirp.addchirp(chirpcalc.calcchirp(300*Math.cos(k)-100,100,2.3+2*Math.sin(k)));
					}
					}).start();
				*/
				long dispt=System.currentTimeMillis();
				d.display();
				System.out.println("Disp  time (ms):"+(System.currentTimeMillis()-dispt));
				
				//Math.sin(j);
			}
		System.out.println("Done");
		
		
	}
	
}
