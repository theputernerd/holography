/**chirpcalc.cpp
  * Project holoGLjniReal
  * Written by J.West
  * May 04
  * This class was written in such a way that it can be called from Java
  * The sole task of this class is to take 3 co-ordinates (x,y,z) and return
  * a single real valued interference pattern. 
  * There is nothing stopping a more complicated interference pattern being generated and returned.
  * 
  *
  */
#include "chirpcalc.h"
#include <GL/glut.h>
#include <math.h>
#define wd 800
#define ht 600
//#define wd 1280  //used for creating large chirp image
//#define ht 1024  //don't forget to change it in displaychirp as well
#define checkImageWidth wd//800
#define checkImageHeight ht//600
#define lambda 632.8e-9
#define delta 10e-6
#define pi 3.142
 
int pixelWidth=wd;//800;  //=800;
int pixelHeight=ht;//600; //=800;
double alphaz=pi*pow(delta,2)/((double)(lambda));
int vertP1=0;//299;//1//min val 1 //value of verticle parralax in pixles. must be less than checkImageHeight
int vertP2=ht;//301;//599 //value of verticle parralax in pixles. must be less than checkImageHeight
double ce[checkImageHeight][checkImageWidth]={0};//={std::complex<double>(255,255)};
	
double* cep=&ce[0][0];      //(std::complex<double>*)malloc(800*600*sizeof(std::complex<double>));//&ce[0][0];
double* cepstart=&ce[0][0]; //the start of the array.
/**
*Returns the complex valued e of the chirp.
*/
double findce(double const screenx, double const screeny, double const *x, double const *y, double const *z)
{	double alpha=alphaz/(*z);//pi*pow(delta,2)/((double)(lambda)*z);   // alpha=t*1/chirpz(cc);
    
	//return &std::complex<double>(cos(((pow((pixelWidth/2.0-(screenx)+(*x)),2)+pow(pixelHeight/2.0-(screeny)+(*y),2)))),0);
	return cos(alpha*((pow((-(screenx)+(*x)),2)+pow(-(screeny)+(*y),2))));
	
	//return &std::polar<double>(1,alpha*((pow((pixelWidth/2.0-(screenx)+(*x)),2)+pow(pixelHeight/2.0-(screeny)+(*y),2))));
	
}



/**Constructs the chirp object
**/
JNIEXPORT jbyteArray JNICALL Java_chirpcalc_calcchirp
  (JNIEnv* env, jclass cl, jdouble x, jdouble y, jdouble z)
{	//printf("Calculating Chirp\n");
	int i,j;
	double tt=0;;
	//*****TO DO: Make this procedure static and have each instance copy it to use it
	//FUNNY NOTE:: Height has to be the first element of the array, which makes sense I suppose
	//since the screen is drawn across.	
	//#include <iostream>
    int count=0;
	
	for(j=0;j<checkImageWidth;++j)
	{
		for(i=vertP1;i<vertP2;i++)
		{
			tt=findce(j,i,&x,&y,&z);
			ce[i][j]=tt;
			
		}
		//removed vertical parralax
	//	for(i=vertP1;i>=0;i--)
		{//parralax
	//		ce[i][j]=ce[i+1][j];
		}
	//	for(i=vertP2;i<=checkImageHeight;i++)
		{
	//		ce[i][j]=ce[i-1][j];
			
		}
	}
		

	jbyteArray  result;//=new jbyteArray(800 * 600);
	jbyte* buf =(jbyte *)cepstart;//new jbyte[800*600];
	//memset(buf, 0,800*600); 
	result=env->NewByteArray(checkImageHeight*checkImageWidth*sizeof(double));
	env->SetByteArrayRegion(result,0,checkImageHeight*checkImageWidth*sizeof(double),buf);
	jsize size = env->GetArrayLength((jarray)result);
//    std::cout<<"SIZE::"<<(size)<<"\n";

//	std::cout<<(*cepstart)<<"\n";  //This is the first term in the array
	delete[] cepstart;
	//delete[] *ce;
	return result;
//I believe chirp calc is working correctly.
//To speed it up maybe could look more at pointers as an option.


}



