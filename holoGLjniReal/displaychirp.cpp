/**displaychirp.cpp
  * Project holoGLjniReal
  * Written by J.West
  * May 04
  * This class was written in such a way that it can be called from Java
  *
  *
  *
  *
  */
#include "displaychirp.h" 
#include <GL/glut.h>
#include <complex>

int mainWindow,gameScreen; 
int x,y,z,h,p,r; //camera angles
int red=0,green=0,blue=0;
int ncols=256;// this is the range of colours

#define wd 800
#define ht 600
//#define wd 1280 //used to create large chirp for hologldepth
//#define ht 1024 //don't forget to change it in displaychirp as well
double min=1;
double max=-1;
int pixelWidth=wd;//800;  //=800;
int pixelHeight=ht;//600; //=800;
#define checkImageWidth wd //800
#define checkImageHeight ht //600


extern pixelWidth;
extern pixelHeight; 
static GLubyte e[checkImageHeight][checkImageWidth][3];
static double (*complexmap)[checkImageWidth]=new double[checkImageHeight][checkImageWidth];
int normalise();
void initialise();	

/**This method empties complexold
**It probably should create a new empty array, so that e can point to it later.
**/
JNIEXPORT void JNICALL Java_displaychirp_newscene(JNIEnv *, jclass)
{   	delete[] complexmap;
	complexmap=new double[checkImageHeight][checkImageWidth];
	max=-1;
	min=1;

	//complexmapnewP=&complexmapnew[0][0];
	

	
}
/**This one displays the chirp and starts opengl
* It never returns so if you want to update the window from java you need to create a java thread
* To do: make this method thread safe.
* Many java instances will be trying to add chirps at once. 
* making complexmap[i][j] thread safe will ensure that the data is written safely.
* Ideally only one value of complexmap will be mutexed, this would allow different threads to write to the same array
* , but different values, hence, the threads will basically be following each other through the array.
**/
JNIEXPORT void JNICALL Java_displaychirp_addchirp
  (JNIEnv *en, jclass jc, jbyteArray jba)
{
//Make this baby thread safe.
	//Lots of instances will try and modify complexmap at the same time
	//so need to protect it.



	//need to calculate the display and store it in e;
	
	//first job of addchirp isa to take the jba and make it into an array of complex

	double* cep=0;
	cep = (double*)((en->GetByteArrayElements(jba,0 )));
	double* cepstart=cep;
	
	//now cep is a pointer that points to the complex 2x2 array.
  //#include <pthread.h>

//	pthread_mutex_t mymutex = PTHREAD_MUTEX_INITIALIZER; 
	//the next job is to add the complex values in this array to the current complex values.
	//in complexmap
	//ONLY ONE LINE OF THE CHIRP HAS BEEN CALCULATED
	//int i=300;
	//Now cep needs to be rotated to get the chirp image.
	//it need to be rotated about the correct point as well..
	//confusing, might do the rotation in chirpcalc and keep this the same.

	for(int i=0;i<checkImageHeight;i++)
	{	for(int j=0;j<checkImageWidth;j++)
		{
			  complexmap[i][j]+=(*cep);

			  cep++;
			  double t=(double)complexmap[i][j];
			  if(t>max)
			  {	 max=t;
			  }
			  if(t<min)
			  {  min=t;
			  }
			
		}
			
	}

	
	//now complexmap has all the complex data for this one image.
	(en)->ReleaseByteArrayElements(jba,(jbyte *)cepstart, 0);
    delete en;
	
	delete[] cepstart;
	

}

/**
*Java calls this when its time to display the new array
*/
JNIEXPORT void JNICALL Java_displaychirp_display
  (JNIEnv *, jclass)
  
{   
	//complexmapnew=complexmap;
	normalise();
}
/**
*Java calls this on initialisation
*/
JNIEXPORT void JNICALL Java_displaychirp_startglut
  (JNIEnv *, jclass)
{
	glutMainLoop();

}


/*
 * Class:     displaychirp
 * Method:    initialise
 * Signature: ()V
 *This one initialises the opengl stuff.
 */
 JNIEXPORT void JNICALL Java_displaychirp_initialise
  (JNIEnv *, jclass)
{

	initialise();
	
    
 }

/**This method normalises the chirp so that the highest value is 256
returned is the value that was the highest
**/
int normalise()
{	//printf("Normalising display .. \n");
	//JNIEnv *en=new JNIEnv;
	//need to extract the array out of joba.
	//std::complex<double>* cep=0;
	
//	printf("Arrays created.. \n");
	
//	printf("Found java Class \n");
	GLubyte c=0;
	double minabs=std::abs<double>(min);
	double maxabs=((double)max);
	int count=0;
	//JOBS TO DO HERE.
	//FIRST ONE is to normalise the values so they go from 0 to 255.
	//Next is to fill the e[][][] array with the real values of that data.


	for(int i=0;i<checkImageHeight;i++)
	{	for(int j=0;j<checkImageWidth;j++)
		{
			  //double tmp=(*cep).real();
	
			  //c= (GLubyte)(((*cep).real()+minabs)*255.0/(minabs+(maxabs)));
			  c=(GLubyte)(((complexmap[i][j])+minabs)*255.0/(minabs+maxabs));
			  //std:complexmap[i][j];
			  //if(c==0) std::cout<<"we have a zero\n";
			  //if(c==1) std::cout<<"we have a one\n";
			 // count++; 
			  e[i][j][0] = c;
			  e[i][j][1] = c;
			  e[i][j][2] = c;
			
		}
			
	}

	
	return max;
	
}

/*************************
below is the opengl functions
****/

/**
Actually shows the display
When show is called e is drawn to the screen.
e should be an 800x600 array of GLBytes
**/
void show()
{   //printf("Show.\n");
		
	glClear  ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) ;

	glRasterPos2i(0, 0);
	glDrawPixels(pixelWidth, pixelHeight, GL_RGB,GL_UNSIGNED_BYTE, e);

	glutSwapBuffers(); //Flush();
	glutPostRedisplay();


}
/**Draws the scene*/
int DrawGLScene(GLvoid)							// Here's Where We Do All The Drawing
{

	//printf("drawing scene");
	show();
	glutPostRedisplay();
	return 1;							// Keep Going

	
}
/**One of opengl callbacks
* for smooth animation change the scene in here
**/
void Display(void)
{  	 //glLoadIdentity();
	 /* Flush the screen buffer to ensure it is displayed. */
	show();
	//glutSwapBuffers () ;
	glutPostRedisplay();
	
}


/*
  The GLUT window reshape event
*/

static void reshape ( int w, int h )
{  
   glViewport(0, 0, (GLint) w, (GLint) h); /* Update the viewport size. */
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
  /* Change the size of the region we are looking at. */
    gluOrtho2D(0.0,(GLdouble) w,0.0,(GLdouble) h);
  /* gluOrtho2D(0.0,(GLdouble) width,0.0,(GLdouble) height); */
   //glMatrixMode(GL_MODELVIEW);
   //glLoadIdentity();
   //glutSwapBuffers () ;
   //glutPostRedisplay();
   show();
   glutPostRedisplay();
  		 
}




/**
*  The GLUT keyboard event
* Even though java has called this, gl still ownes the keyboard events.
* If you wanted the keyboard to manipulate the scene then you'd need to let java know
* a key has been pressed.
* If you just want to rotate or translate the object then it can be done in here.
* If you wanted new objects or new calculations then java needs to know.
*/
static void keyboard ( unsigned char key, int, int )
{
	switch (key)
	{	case 'r':
			printf("Redrawing");
			show();
			break;
		case 'o':
			printf("Swapping buffers");
			glutSwapBuffers () ;
			
			break;
		case 'n':
			printf("normalising");
			normalise();
			show();

			break;
		case 'q':
				glutReshapeWindow(800,600);

		break;
		case 'f':
				glutFullScreen();
		break;
		case 'c':
			for(int i=0;i<checkImageHeight;i++)
			{	for(int j=0;j<checkImageWidth;j++)
				{
			  	complexmap[i][j]=0;	   
				e[i][j][0] = (GLubyte)0;
				e[i][j][1] = (GLubyte)0;
				e[i][j][2] = (GLubyte)0;
			    max=-1;
				min=1;
				}
			
			}
		break;
		

		glutPostRedisplay();
			
		
	}
}

/**
*Initialse the GLwindow
*/
void initialise()
{ 

  int   fake_argc = 1;
  char *fake_argv[3];
  fake_argv[0] = "Holography Image" ;
  fake_argv[1] = "Version 1" ;
  fake_argv[2] = NULL ;
  printf("Initialising GLUT..");
  /*
    Initialise GLUT
  */
  glutInitDisplayMode    ( GLUT_RGB | GLUT_DOUBLE  ) ;
  
  glutInitWindowPosition ( 0, 0 ) ;
  glutInitWindowSize     ( checkImageWidth, checkImageHeight ) ;
 // glutInit               ( &fake_argc, fake_argv ) ;
 // glutInitDisplayMode    ( GLUT_RGB  ) ;
  mainWindow=glutCreateWindow       ( fake_argv[1] ) ;
  glutDisplayFunc        ( Display   ) ;
  glutReshapeFunc        ( reshape  ) ;
  glutKeyboardFunc       ( keyboard ) ;
  
 // printf(" ..GLUT Initialise success..\n");
  
  /*
    Some basic OpenGL setup
  */

  



}


