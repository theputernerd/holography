/**
*This class provides the interface betweeen java and the 
*C++ opengl calls.
*When called the given object is dislayed.
* The object should be taken directly from one of the other native classes
in this project.
*Further features will include the ability to add to the screen
**/
class displaychirp
{
	public  static native void display();
	public  static native void addchirp(byte[] scrn);
	public  static native void initialise();
	public  static native void startglut();
	public  static native void newscene();
	
	static
	{
		System.loadLibrary("displaychirp");
	}

}