  import java.lang.Math.*;
  /**master.java
  * Project holoGLjniReal
  * Written by J.West
  * May 04
  * 
  * This class is the java executable for the project holoGLjniReal.
  * In this class the object's points are defined. 
  * Required libraries are displaychirp.dll, chirpcalc.dll glut32.dll
  * 
  * To customise this program the following procedure should be used:
  *initialise display and start opengl in its own java thread.
  *displaychirp.newscene(); to clear the scene
  *Create chirp instances, and calculate points.
  *Pass the return directly to addchirp.
  *call d.display() to move the new array to the framebuffer
  */
public class master
{	static byte[] singlechirp;
	static double k;
	public static void main(String args[])
	{  
		
  		long chirpcalct=System.currentTimeMillis();
		byte[] chirp2;
		singlechirp=chirpcalc.calcchirp(0,-200,.5);
		chirpcalct=System.currentTimeMillis()-chirpcalct;
		
		//System.out.println("value "+singlechirp);
		System.out.println("1 Chirp calc time (ms):"+chirpcalct);
		//chirp2=chirpcalc.calcchirp(0,200,.5);
		final displaychirp d=new displaychirp();	
		
           new Thread() {
    		public void run() 
    		{   d.initialise();
				d.startglut();
		   	
        	    	  
    		}
  			}.start();
  		//dispt=System.currentTimeMillis()-dispt;
		System.out.println("outside thread");
	chirpcalct=System.currentTimeMillis();
			 //for( k=0;k<8*Math.PI;k=k+.05)
			{   displaychirp.newscene();
			 	//d.addchirp(chirpcalc.calcchirp(400,300,.01));
			 	//d.addchirp(chirpcalc.calcchirp(300*Math.sin(k)-100,200*Math.cos(k)+50,50));
				//d.addchirp(chirpcalc.calcchirp(300*Math.sin(k)-100,200*Math.cos(k)+50,10));
				//d.addchirp(chirpcalc.calcchirp(0,200*Math.sin(k)+100,2.3+2*Math.cos(k)));
				//d.addchirp(chirpcalc.calcchirp(300*Math.cos(k)-100,100,2.3+2*Math.sin(k)));
				
				//d.addchirp(chirpcalc.calcchirp(700,500,.4));				
			 	d.addchirp(chirpcalc.calcchirp(-60,900,.4));  //used to create large chirp for hologldepth				
			 	chirpcalct=System.currentTimeMillis()-chirpcalct;
				System.out.println("2 Chirp calc time (ms):"+chirpcalct);
		
			 	//d.addchirp(chirpcalc.calcchirp(330,300,1));				
			 	//d.addchirp(chirpcalc.calcchirp(350,300,1));				
			 	//d.addchirp(chirpcalc.calcchirp(380,300,1));				
			 	//d.addchirp(chirpcalc.calcchirp(400,300,1));				
			 	//d.addchirp(chirpcalc.calcchirp(420,300,1));				
			 	//d.addchirp(chirpcalc.calcchirp(440,300,1));				
			 	//d.addchirp(chirpcalc.calcchirp(460,300,1));				
			 	//d.addchirp(chirpcalc.calcchirp(1280/2,1024/2,.1));				
			 	/*new Thread(new Runnable() 
			 	 {
    				public void run() 
    				{ displaychirp.addchirp(chirpcalc.calcchirp(300*Math.sin(k)-100,200*Math.cos(k)+50,1));
					  displaychirp.display();
					}
				}).start();
				*/
				
				//try{Thread.sleep(1000);
				//}
				//catch(Exception e)
				//{
				//}
				
				
			/*	new Thread(new Runnable() 
			 	 {public void run() 
    				{displaychirp.addchirp(chirpcalc.calcchirp(0,200*Math.sin(k)+100,2.3+2*Math.cos(k)));
					}
					}).start(); 
				new Thread(new Runnable() 
			 	 {public void run() 
    				{displaychirp.addchirp(chirpcalc.calcchirp(300*Math.cos(k)-100,100,2.3+2*Math.sin(k)));
					}
					}).start();
				*/
				long dispt=System.currentTimeMillis();
				d.display();
				System.out.println("Chirp display time (ms):"+(System.currentTimeMillis()-dispt));
		
				//Math.sin(j);
			}
		System.out.println("Done");
		
		
	}
	
}
