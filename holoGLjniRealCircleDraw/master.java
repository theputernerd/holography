
  import java.lang.Math.*;
  /**master.java
  * Project holoGLjniReal
  * Written by J.West
  * May 04
  * 
  * This class is the java executable for the project holoGLjniReal.
  * In this class the object's points are defined. 
  * Required libraries are displaychirp.dll, chirpcalc.dll glut32.dll
  * 
  * To customise this program the following procedure should be used:
  *initialise display and start opengl in its own java thread.
  *displaychirp.newscene(); to clear the scene
  *Create chirp instances, and calculate points.
  *Pass the return directly to addchirp.
  *call d.display() to move the new array to the framebuffer
  */
public class master
{	static byte[] singlechirp;
	static double k;
	public static void main(String args[])
	{   //I wonder if I start this in a thread and playwith the chirps will it change the display
	final displaychirp d=new displaychirp();	
			
		
  long chirpcalct=System.currentTimeMillis();
		byte[] chirp2;
		chirpcalc c=new chirpcalc();
		singlechirp=c.calcchirp(0,-200,.5);
		chirpcalct=System.currentTimeMillis()-chirpcalct;
		
		//System.out.println("value "+singlechirp);
		long dispt=System.currentTimeMillis();
		System.out.println("1 Chirp calc time (ms):"+chirpcalct);
		//chirp2=chirpcalc.calcchirp(0,200,.5);
		
           new Thread() {
    		public void run() 
    		{   d.initialise();
				d.startglut();
		   	
        	    	 
    		}
  			}.start();
  		//dispt=System.currentTimeMillis()-dispt;
		System.out.println("outside thread");
		
			//for( k=0;k<8*Math.PI;k=k+.05)
			{   d.newscene();
			 	 c=new chirpcalc();
		
			 	//d.addchirp(c.calcchirp(400+400*Math.sin(k),300+300*Math.cos(k),1));
			 	d.addchirp(c.calcchirp(100,100,1));
			 	//d.addchirp(c.calcchirp(0,100,1));
			 	//d.addchirp(chirpcalc.calcchirp(400,300+300*Math.cos(k),1.5+3*Math.sin(k)));
			 	//d.addchirp(chirpcalc.calcchirp(400+400*Math.sin(k),300,1.5+3*Math.sin(k)));
			 	
			 	
			 	//d.addchirp(chirpcalc.calcchirp(300*Math.sin(k)-100,200*Math.cos(k)+50,1));
				//d.addchirp(chirpcalc.calcchirp(0,200*Math.sin(k)+100,2.3+2*Math.cos(k)));
				//d.addchirp(chirpcalc.calcchirp(300*Math.cos(k)-100,100,2.3+2*Math.sin(k)));
								
			 	/*new Thread(new Runnable() 
			 	 {
    				public void run() 
    				{ displaychirp. addchirp(chirpcalc.calcchirp(300*Math.sin(k)-100,200*Math.cos(k)+50,1));
					  displaychirp.display();
					}
				}).start();
				*/
				
				//try{Thread.sleep(1000);
				//}
				//catch(Exception e)
				//{
				//}
				
				
			/*	new Thread(new Runnable() 
			 	 {public void run() 
    				{displaychirp.addchirp(chirpcalc.calcchirp(0,200*Math.sin(k)+100,2.3+2*Math.cos(k)));
					}
					}).start(); 
				new Thread(new Runnable() 
			 	 {public void run() 
    				{displaychirp.addchirp(chirpcalc.calcchirp(300*Math.cos(k)-100,100,2.3+2*Math.sin(k)));
					}
					}).start();
				*/
				d.display();
				
				//Math.sin(j);
			}
		System.out.println("Done");
		
		
	}
	
}
