/*chirp.cpp
*Chirp class file.
*Version 3 - In this version you can reduce the amount of vertical parrax with the vertP1 and vertP2 variables
*The actual positions of the chirps need to be "calibrated" to the optical system in use.
*
*This is not set up for animations, however they can easily be added. Required is a function to clear the complex array
*and clean up the variables.
*
*/
#include "chirp.h"
#include <time.h>
int chirp::pixelWidth=800;  //=800;
int chirp::pixelHeight=600; //=800;
int chirp::vertP1=0; //399//0//value of verticle parralax in pixles. must be less than checkImageHeight
int chirp::vertP2=600; //401//600//value of verticle parralax in pixles. must be less than checkImageHeight
double chirp::alphaz=pi*pow(delta,2)/((double)(lambda));	
double start,finish;
	
/**
* Shows the chirp to the screen, display Etot
*/
chirp::show()
{
    double duration;
    
    start = clock();


	
	glClear  ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) ;

	//glClear(GL_COLOR_BUFFER_BIT);
	glRasterPos2i(0, 0);
	glDrawPixels(pixelWidth, pixelHeight, GL_RGB,GL_UNSIGNED_BYTE, e);



	glutSwapBuffers(); //Flush();
	finish = clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
	//printf("\tDrawn in %f seconds",duration);
	//complex<double>(1,3);

}

/**Sets the value of a byte in the array - for testing purposes
**/
void chirp::set(int x, int y, int val)
{
	e[x][y][0] = (GLubyte) val;
	e[x][y][1] = (GLubyte) val;
	e[x][y][2] = (GLubyte) val;


}

/**This method normalises the chirp so that the highest value is 256
returned is the value that was the highest
**/
int chirp::normalise()
{	
	GLubyte c;
	double tmax=0;
	double tmin=10;
	double cd=0;
	double minabs=std::abs<double>(min);
	double maxabs=((double)max);
	for(int i=0;i<checkImageHeight;i++)
	{	for(int j=0;j<checkImageWidth;j++)
		{
			c= (GLubyte)((ce[i][j].real()+minabs)*255.0/(minabs+(maxabs)));
			
			
			e[i][j][0] = c;
			e[i][j][1] = c;
			e[i][j][2] = c;
			
					//printf("(%f, %i)",eUnNorm[i][j][0],(GLubyte)(256*(eUnNorm[i][j][0])/max));
		}
	}
	//printf("max %f , min %f \n",max,min);
	 
	return max;
}


int chirp::normE()
{
	
	return max;

}
int chirp::normalise(int value)
{
	
	return max;
}
/**
*Returns the complex valued e of the chirp.
*/
std::complex<double> *chirp::findce(double const screenx, double const screeny, double const *x, double const *y, double const *z)
{	double alpha=alphaz/(*z);//pi*pow(delta,2)/((double)(lambda)*z);   // alpha=t*1/chirpz(cc);
	//std::cout<<alpha*((pow((pixelWidth/2.0-(screenx)+(*x)),2)+pow(pixelHeight/2.0-(screeny)+(*y),2)));
	return &std::polar<double>(1,alpha*((pow((pixelWidth/2.0-(screenx)+(*x)),2)+pow(pixelHeight/2.0-(screeny)+(*y),2))));
	
	
}


/**Adds a chirp to the current scene
**/
void chirp::addchirp(double const x, double const y, double const z)
{
	
	int i,j;
	double c;//,mat;
	std::complex<double> tt;
	//*****TO DO: Make this procedure static and have each instance copy it to use it
	//FUNNY NOTE:: Height has to be the first element of the array, which makes sense I suppose
	//since the screen is drawn across.
	for(j=0;j<checkImageWidth;j++)
	{
		
		for(i=vertP1;i<vertP2;i++)
		{	tt=*findce(i,j,&x,&y,&z);
			//std::cout<<*findce(i,j,x,y,z);
			//printf("%f ",tt);
			ce[i][j]+=tt;//tt.real()+tt.imag();

			//ce[i][j]+=findce(i,j,x,y,z); //findce(i,j,x,y,z);
			c=ce[i][j].real();
			
			if(c>max) max=c;

			if(c<min) min=c;
	

		}
		for(i=vertP1-1;i>=0;i--)
		{
			ce[i][j]=ce[i+1][j];
		}
		for(i=vertP2-1;i<=checkImageHeight;i++)
		{
			ce[i][j]=ce[i-1][j];
			
		}
	}

}
/**Primary constructor for the chirp
**/	
chirp::chirp(const short pixelwidth,double xposition, double yposition, double zposition, int resolution)
{ 	chirp::res=resolution;
 	min=0;
	max=0;
	int i,j;
	double c=0;
	//*****TO DO: Make this procedure static and have each instance copy it to use it
	//FUNNY NOTE:: Height has to be the first element of the array, which makes sense I suppose
	//since the screen is drawn across.
	//Initialises the array
	std::complex<double> tt=std::polar<double>(0,0);
	
	for(i=0;i<checkImageHeight;i++)
	{
		for(j=0;j<checkImageWidth;j++)
		{
			e[i][j][0] = (GLubyte) c;  //these are the same to give a gray value
			e[i][j][1] = (GLubyte) c;
			e[i][j][2] = (GLubyte) c;
			ce[i][j]=tt;
			
		}
	}
	





}


	