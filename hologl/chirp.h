/**
*This class will calculate the gray values for a chirp.
*
*/
#ifndef _chirp_H_
#define _chirp_H_

#include <complex>
#include <GL/glut.h>

#define checkImageWidth 800
#define checkImageHeight 600
#define lambda 632.8e-9
#define delta 10e-6
#define pi 3.142
class chirp
{
public:
	/**Main constructor
	*Resolution is the number of chirps that will be on the screen. This designates the maximum brightness value.
	
	**/
	chirp(const short pixelsize,double posx, double posy, double posz, int resolution);
	/*E(xx,yy)=1/(chirpz(cc))*exp(i*alpha*((xaxis(xx)+chirpx(cc)).^2+(yaxis(yy)+chirpy(cc)).^2));%+exp(i*alpha2*((xaxis(xx)+chirpx(cc)).^2+(yaxis(yy)+chirpy(cc)).^2))+exp(i*alpha3*((xaxis(xx)+chirpx(cc)).^2+(yaxis(yy)+chirpy(cc)).^2)));
     */

	static int pixelWidth;  //=800;
	static int pixelHeight; //=800;
	static int vertP1;//=299; //value of verticle parralax in pixles. must be less than checkImageHeight
	static int vertP2;//=301; //value of verticle parralax in pixles. must be less than checkImageHeight
	static double alphaz;
					  /**displays the complete image, Etot, to the screen
	**/
	show();
	/**This variable is the complete screen with all chirps added to it.
	**/
	//std::vector <std::vector <GLubyte> > Etot;
	//std::vector <std::vector <double>> Etot; 

	/**This is the single chirp being calculated
	**/
	//std::vector <std::vector <std::vector<GLubyte> > > e;

	std::complex<double> *chirp::findce(double const screenx, double const screeny, double const *x, double const *y, double const *z);
	//GLubyte checkImage[30][30][3];
	
	//GLubyte checkImage[checkImageHeight][checkImageWidth][3];
	
	GLubyte e[checkImageHeight][checkImageWidth][3];
	std::complex<double> ce[checkImageHeight][checkImageWidth];
	std::complex<double> ceOneLine[checkImageWidth];
	
	void addchirp(double const x, double const y, double const z);
	
	int res;
	
	int chirp::normalise();
	/**Normalises the array with the given value
	**/
	int chirp::normalise(int value);
	int chirp::normE(); //goes the others normalise doubles, this normalises e with glubyte
	double max,min; //hold the maximum value of a pixel, used for normalisation
	
	void set(int x, int y, int val);
	//static double alpha;   //requires z
	//std::vector <std::vector <double> > e;

private:

//	for(i=0;i<pixelwidth;i++)
//	{//creates a vector of vectors.
		   //to fill the vectors now use e[0].push_back(15.0); //e[0][0]=15
		   //they have to be put in in order. to use then simply e[0][0];
//	   e.push_back(std::vector <fpolar>()); /*create e[0]*/
//	}

    /**The number of pixels the chirp drawn to. will be sqaure initially
	**/
	short pixelx;
	/**The number of pixels the chirp drawn to in y*/
	short pixely;

	
    /**The position of the chirp in x,y,z space.
	+ve x is to the right
	+ve y is up
	+z is into the screen
	0,0,0 is in the center of the screen at the viewing plane
	**/
	double posx,posy,posz;
	
};


#endif

