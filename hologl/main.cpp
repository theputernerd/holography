#include <GL/glut.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include <stdio.h>
#include <stdlib.h>
#include "chirp.h"
#include <math.h>
#include<time.h>
int mainWindow,gameScreen; 
int x,y,z,h,p,r; //camera angles
int red=0,green=0,blue=0;
int ncols=256;// this is the range of colours
chirp *c;
double startt,finisht;


int DrawGLScene(GLvoid)							// Here's Where We Do All The Drawing
{

	//printf("drawing scene");
	c->show();
	glutPostRedisplay();
	return 1;							// Keep Going

	
}

void Display(void)
{  	 //glLoadIdentity();
	 /* Flush the screen buffer to ensure it is displayed. */
	//for animations do them in here


	c->show();
	glutPostRedisplay();
	
}


/*
  The GLUT window reshape event
*/

static void reshape ( int w, int h )
{  
   glViewport(0, 0, (GLint) w, (GLint) h); /* Update the viewport size. */
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
  /* Change the size of the region we are looking at. */
    gluOrtho2D(0.0,(GLdouble) w,0.0,(GLdouble) h);
 
	c->show();
   glutPostRedisplay();
  		 
}



static void redraw ()
{
  //Change the camera accordingly

  

 c->show();
 glutPostRedisplay();
	
  

  
}
/*
  The GLUT keyboard event
*/

static void keyboard ( unsigned char key, int, int )
{
	switch (key)
	{	case 'i':
			printf("%i,%i,%i\n",red,green,blue);
			break;

		case 'r':
			printf("Redrawing");
			c->show();
			glutPostRedisplay();
			break;
		case 'o':
			printf("Swapping buffers");
			glutSwapBuffers () ;
			break;
		
	}
}


void initialise()
{ //x=0;y=30;z=-40;h=-127;p=90;r=-53; 
//  x=-1;y=0;z=0;h=0;p=0;r=0;
  //carx=0;cary=0;carz=0;
  int   fake_argc = 1 ;
  char *fake_argv[3] ;
  fake_argv[0] = "HoloGL" ;
  fake_argv[1] = "Version 3" ;
  fake_argv[2] = NULL ;

  /*
    Initialise GLUT
  */
  glutInit               ( &fake_argc, fake_argv ) ;
 	
  glutInitDisplayMode    ( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH ) ;
  
  glutInitWindowPosition ( 0, 0 ) ;
  glutInitWindowSize     ( 800, 600 ) ;
  mainWindow=glutCreateWindow       ( fake_argv[1] ) ;
  glutDisplayFunc        ( &Display   ) ;
  glutReshapeFunc        ( &reshape  ) ;
  glutKeyboardFunc       ( &keyboard ) ;
  glutIdleFunc(Display);
	
  
  /*
    Some basic OpenGL setup
  */

  



}

int main(int argc, char **argv)
{   initialise();
		printf("%i",glutGet(GLUT_WINDOW_DEPTH_SIZE));
    double * xaxis=new double[600];  
    double * yaxis=new double[600];  
	//int y[]=(*double)malloc(5*sizeof(double));//new double[xaxis][yxaxis];
	xaxis[0]=1;
	xaxis[1]=2;
	c=new chirp(800,0,0,0,1);
	startt = clock();
	//double e[sizeof(xaxis)][sizeof(xaxis)];
	//e[599][599]=1;
	
	
	
	//Puts a line on the screen
	
	int res=5;
	printf("\n%d\n",sizeof(xaxis));
    

	for(int i=0;i<res;i++)
	{
		
		c->addchirp(-300+i*600/res,0,.5+i*1/1000);
		
	}

	
	//Puts s 2d surface on the screen

	//first draw the line
	//c->addchirp(0,0,.5);
/*	double tt1=new double;*tt1=400/130;
	double tt2=new double;*tt2=400/130;//=
	double tt3=new double;
	double tt4=new double;
	double tt5=new double;
	
	tt5=.05;
	int xsqr=2,ysqr=2;
	int i,j;
	//*tt2=.02;
	for(i=0;i<xsqr;i++)
	{	for(j=0;j<ysqr;j++)
		{	tt3=-200.0+(tt1)*j;
			tt4=(600.0+(tt2)*i);
			
			c->addchirp(tt3,tt4,tt5);
		}
		
	}
*/
/*	double tt3=new double; //these are pointers to speed things up
	double tt4=new double; //probably overkill
	double tt5=new double;
	double tt6=new double; //these are pointers to speed things up
	double tt7=new double;
	double tt8=new double;
	tt3=-100;tt4=-100;tt5=.2;
	tt6=10;tt7=10;tt8=.2;

 	c->addchirp(tt3,tt4,tt5);   //just puts up 5 chirps so I can calculate the speed
	c->addchirp(tt6,tt7,tt5);
	c->addchirp(tt6,tt7,tt5);
	c->addchirp(tt6,tt7,tt5);
	c->addchirp(tt6,tt7,tt5);
*/
	c->normalise();
//	c->show();
	//GLubyte a;
	//now the line exists just need to shift it to the left or right to make a surface
	
/*Trying to repaint the pattern accross the screen In reality need more information outside the screen to do it.
	for(int k=0;k<80;k++)
	{
	 	for(i=0;i<checkImageHeight;i++)
		{	for(int j=k;j<checkImageWidth;j++)
			{	a=c->eCopy[i][j-k][0];

//				c=(GLubyte) (256*eUnNorm[i][j][0]/max);
				//printf("(%f, %i)",eUnNorm[i][j][0],(GLubyte)(256*(eUnNorm[i][j][0])/max));
				//c->set(i,j,a);
				//printf("(%i+%f=",c->e[i][j][0],c->eUnNorm[i][j-k][0]);
				
				c->eUnNorm[i][j][0] += a;
				c->eUnNorm[i][j][1] += a;
				c->eUnNorm[i][j][2] += a;
				
				if(c->eUnNorm[i][j][0]>(c->max))
				{
					c->max=(c->eUnNorm[i][j][0]);
					printf(" %f, ",c->max);
				}
				//c->set(i,j,255);
				//printf("%f)",c->eUnNorm[i][j][0]);
				

			}
		
			for(int jj=0;jj<k;jj++)
			{

				c->e[i][jj][0] += 0;
				c->e[i][jj][1] += 0;
				c->e[i][jj][2] += 0;

			}
		}
	
		
	}	


*/



	//	printf(" MAXIMUM %f, ",c->max);

	//c->normalise();

	finisht=clock();
	printf("Calculation time: %f sec\n",(finisht-startt)/CLOCKS_PER_SEC);

	//	c->addchirp(150,0,.1);
	
	glutMainLoop();
    return 0;
}

