#include "chirpcalc.h"
#include <GL/glut.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include <iostream>
#include <complex>
#include <math.h>
#define checkImageWidth 800
#define checkImageHeight 600
#define lambda 632.8e-9
#define delta 10e-6
#define pi 3.142
 
int pixelWidth=800;  //=800;
int pixelHeight=600; //=800;
double alphaz=pi*pow(delta,2)/((double)(lambda));
int vertP1=1;//299;//1//min val 1 //value of verticle parralax in pixles. must be less than checkImageHeight
int vertP2=599;//301;//599 //value of verticle parralax in pixles. must be less than checkImageHeight
double ce[checkImageHeight][checkImageWidth]={0};//={std::complex<double>(255,255)};
	
double* cep=&ce[0][0];      //(std::complex<double>*)malloc(800*600*sizeof(std::complex<double>));//&ce[0][0];
double* cepstart=&ce[0][0]; //the start of the array.
/**
*Returns the complex valued e of the chirp.
*/
double findce(double const screenx, double const screeny, double const *x, double const *y, double const *z)
{	double alpha=alphaz/(*z);//pi*pow(delta,2)/((double)(lambda)*z);   // alpha=t*1/chirpz(cc);
    
	//return &std::complex<double>(cos(((pow((pixelWidth/2.0-(screenx)+(*x)),2)+pow(pixelHeight/2.0-(screeny)+(*y),2)))),0);
	return cos(alpha*((pow((-(screenx)+(*x)),2)+pow(-(screeny)+(*y),2))));
	
	//return &std::polar<double>(1,alpha*((pow((pixelWidth/2.0-(screenx)+(*x)),2)+pow(pixelHeight/2.0-(screeny)+(*y),2))));
	
}



/**
*Draws a circle in the array Centered at checkImageHeight/2,checkImageWidth/2
*/
void circlePoints(int w, int h,int x, int y, double value)
{

    bool hpx=((h+x)<checkImageHeight)&((h+x)>0);
	bool hmx=(h-x)>0;
	bool hpy=((h+y)<checkImageHeight)&((h+y)>0);
	bool hmy=(h-y)>0;
	bool wpx=((w+x)<checkImageWidth)&((w+x)>0);
	bool wmx=(w-x)>0;
	bool wpy=((w+y)<checkImageWidth)&((w+x)>0);
	bool wmy=(w-y)>0;
	
	if(hpx&wpy)
		ce[h+x][w+y]=value;
	if(hpy&wpx)
		ce[h+y][w+x]=value;
	if(hpy&wmx)
		ce[h+y][w-x]=value;
	if(hpx&wmy)
		ce[h+x][w-y]=value;
	if(hmx&wmy)
		ce[h-x][w-y]=value;
	if(hmy&wmx)
		ce[h-y][w-x]=value;
	if(hmy&wpx)
		ce[h-y][w+x]=value;
	if(hmx&wpy)
		ce[h-x][w+y]=value;

	
}

void circlePointsOrigin(int x, int y, double value)
{
	ce[+x][+y]=value;
	ce[+y][+x]=value;
	ce[+y][-x]=value;
	ce[+x][-y]=value;
	ce[-x][-y]=value;
	ce[-y][-x]=value;
	ce[-y][+x]=value;
	ce[-x][+y]=value;


}
/**Assume circle is at origin
*radius should be <500 for 800*600 window
*/
void midPointCircle(int xc,int yc, int radius,double value)
{	int x=0;
	int y=radius;
	int d=1-radius;
	int deltaE=3;
	int deltaSE=-2*radius+5;

	circlePoints(xc,yc,x,y,value);

	while(y>x)
	{
		if(d<0)
		{
			d+=deltaE;
			deltaE+=2;
			deltaSE+=2;
		}
		else
		{
			d+=deltaSE;
			deltaE+=2;
			deltaSE+=4;
			y--;
		}
		x++;
		circlePoints(xc,yc,x,y,value);
	}//while

}//midpointcircle

JNIEXPORT jbyteArray JNICALL Java_chirpcalc_calcchirp
  (JNIEnv* env, jclass cl, jdouble x, jdouble y, jdouble z)
{	//printf("Calculating Chirp\n");
	int i,j;
//	glutInitDisplayMode    ( GLUT_RGB | GLUT_DOUBLE  ) ;
 //   glClear  ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) ;
//	glDrawBuffer(GL_BACK);
	
	//double c;//,mat;
	double tt=0;;
	//*****TO DO: Make this procedure static and have each instance copy it to use it
	//FUNNY NOTE:: Height has to be the first element of the array, which makes sense I suppose
	//since the screen is drawn across.	
	//#include <iostream>
    int count=0;
	
	for(j=0;j<checkImageWidth;++j)
	{
		for(i=vertP1;i<vertP2;i++)
		{
			tt=findce(j,i,&x,&y,&z);
			ce[i][j]=tt;
			
		}

		for(i=vertP1;i>=0;i--)
		{//parralax
			ce[i][j]=ce[i+1][j];
		}
		for(i=vertP2;i<=checkImageHeight;i++)
		{
			ce[i][j]=ce[i-1][j];
			
		}
	}
		

	jbyteArray  result;//=new jbyteArray(800 * 600);
	jbyte* buf =(jbyte *)cepstart;//new jbyte[800*600];
	//memset(buf, 0,800*600); 
	result=env->NewByteArray(800*600*sizeof(double));
	env->SetByteArrayRegion(result,0,800*600*sizeof(double),buf);
	jsize size = env->GetArrayLength((jarray)result);
//    std::cout<<"SIZE::"<<(size)<<"\n";

//	std::cout<<(*cepstart)<<"\n";  //This is the first term in the array
	delete[] cepstart;
	//delete[] *ce;
	return result;
//I believe chirp calc is working correctly.
//To speed it up maybe could look more at pointers as an option.


}



