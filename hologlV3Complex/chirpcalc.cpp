#include "chirpcalc.h"

#include <iostream>
#include <complex>
#include <math.h>
#define checkImageWidth 800
#define checkImageHeight 600
#define lambda 632.8e-9
#define delta 10e-6
#define pi 3.142
 
int pixelWidth=800;  //=800;
int pixelHeight=600; //=800;
double alphaz=pi*pow(delta,2)/((double)(lambda));
int vertP1=1;//299;//1//min val 1 //value of verticle parralax in pixles. must be less than checkImageHeight
int vertP2=599;//301;//599 //value of verticle parralax in pixles. must be less than checkImageHeight
std::complex<double> ce[checkImageHeight][checkImageWidth];//={std::complex<double>(255,255)};
	
std::complex<double>* cep=&ce[0][0];      //(std::complex<double>*)malloc(800*600*sizeof(std::complex<double>));//&ce[0][0];
std::complex<double>* cepstart=&ce[0][0]; //the start of the array.
/**
*Returns the complex valued e of the chirp.
*/
std::complex<double> *findce(double const screenx, double const screeny, double const *x, double const *y, double const *z)
{	double alpha=alphaz/(*z);//pi*pow(delta,2)/((double)(lambda)*z);   // alpha=t*1/chirpz(cc);
    
	//return &std::complex<double>(cos(((pow((pixelWidth/2.0-(screenx)+(*x)),2)+pow(pixelHeight/2.0-(screeny)+(*y),2)))),0);

	return &std::polar<double>(1,alpha*((pow((pixelWidth/2.0-(screenx)+(*x)),2)+pow(pixelHeight/2.0-(screeny)+(*y),2))));
	
}


JNIEXPORT jbyteArray JNICALL Java_chirpcalc_calcchirp
  (JNIEnv* env, jclass cl, jdouble x, jdouble y, jdouble z)
{	//printf("Calculating Chirp\n");
	int i,j;
	
	//double c;//,mat;
	std::complex<double> tt=0;;
	//*****TO DO: Make this procedure static and have each instance copy it to use it
	//FUNNY NOTE:: Height has to be the first element of the array, which makes sense I suppose
	//since the screen is drawn across.	
	//#include <iostream>
    int count=0;
	
		
	for(j=0;j<checkImageWidth;j++)
	{
		
		for(i=vertP1;i<vertP2;i++)
		{
	//for(i=0;i<checkImageHeight;i++)
	//{	//printf("%f,",*(cep+i+j));
	//	for(j=0;j<checkImageWidth;j++)
	//	{	//Seems that cep is zero at this point.
			//this seems to be correct as cep is pointing to some address.
			
			ce[i][j]=*findce(i,j,&x,&y,&z);;//tt.real()+tt.imag();
						
			//count++;
			//std::cout<<ce[i][j];
			//*cep and ce[i][j] show the same values;
			//Can use either ce[i][j]=tt; or //*(cep)=tt; to fill the array.
			//cep++;
			
		}

		for(i=vertP1;i>=0;i--)
		{
			ce[i][j]=ce[i+1][j];
		}
		for(i=vertP2;i<=checkImageHeight;i++)
		{
			ce[i][j]=ce[i-1][j];
			
		}



		
	}
	//printf("Counter::%i",count);
		
	
	jbyteArray  result;//=new jbyteArray(800 * 600);
	jbyte* buf =(jbyte *)cepstart;//new jbyte[800*600];
	//memset(buf, 0,800*600); 
	result=env->NewByteArray(800*600*sizeof(std::complex<double>));
	env->SetByteArrayRegion(result,0,800*600*sizeof(std::complex<double>),buf);
	jsize size = env->GetArrayLength((jarray)result);
//    std::cout<<"SIZE::"<<(size)<<"\n";

//	std::cout<<(*cepstart)<<"\n";  //This is the first term in the array
	delete[] cepstart;
	//delete[] *ce;
	return result;
//I believe chirp calc is working correctly.
//To speed it up maybe could look more at pointers as an option.


}



