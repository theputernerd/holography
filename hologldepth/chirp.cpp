#include "chirp.h"

int chirp::pixelWidth=800;  //=800;
int chirp::pixelHeight=600; //=800;
int chirp::vertP1=0; //0//value of verticle parralax in pixles. must be less than checkImageHeight
int chirp::vertP2=600; //600//value of verticle parralax in pixles. must be less than checkImageHeight
double chirp::alphaz=pi*pow(delta,2)/((double)(lambda));	

const char *chirp::getTypeName(void) 
{ 
	return "chirp"; 
}

void chirp::copy_from ( chirp *src, int clone_flags )
{
	ssgBranch::copy_from ( src, clone_flags ) ;
}
int chirp::load ( FILE *fd )
{
	return ssgBranch::load(fd) ;
}

int chirp::save ( FILE *fd )
{
	return ssgBranch::save(fd) ;
}

ssgBase *chirp::clone ( int clone_flags )
{
	chirp *b = new chirp ;
	b -> copy_from ( this, clone_flags ) ;
	return b ;
}

/**
*Returns the complex valued e of the chirp.
*/
std::complex<double> *chirp::findce(double const screenx, double const screeny, double const *x, double const *y, double const *z)
{	double alpha=alphaz/(*z);//pi*pow(delta,2)/((double)(lambda)*z);   // alpha=t*1/chirpz(cc);
    
	return &std::polar<double>(1,alpha*((pow((pixelWidth/2.0-(screenx)+(*x)),2)+pow(pixelHeight/2.0-(screeny)+(*y),2))));
	
	//val+=std::polar<double>(1,alpha*((pow((pixelWidth/2.0-screenx+x),2)+pow(pixelHeight/2.0-screeny+y,2))));
	//return val;
}

/**For now this just puts a chirp on a vertical plane at the center
**in future it will allow construction in other places.
*/
chirp::chirp(void)
{	
	ssgTransform *heightAdjust = new ssgTransform; 
	this->addKid(heightAdjust);
	m_Xfm[0] = heightAdjust;



}

chirp::~chirp (void)
{
	delete(this);

}