#ifndef _Chirp_H_
#define _Chirp_H_

#include <plib/ssg.h>
#include <plib/sg.h>

//#include <ul/ul.h>
//#include "sgd/sgd.h"
#include <complex>

#define checkImageWidth 800
#define checkImageHeight 600
#define lambda 650e-9
#define delta 10e-6
#define pi 3.142

class chirp: public ssgBranch
{
public:


	/**
	**\brief Required when extending ssgBranch<br>
	*/
	virtual ssgBase *clone ( int clone_flags = 0 );

	std::complex<double> *chirp::findce(double const screenx, double const screeny, double const *x, double const *y, double const *z);

	
	/**
	**\brief primary constructor.<br>
	*/
	chirp (void) ;

	/**
	**\brief primary destructor.
	*/
	virtual ~chirp (void) ;
	/**
	**\brief Override ssgBranch methods.
	*/
	virtual const char *getTypeName(void) ;
	/**
	**\brief Override ssgBranch methods.
	*/
	virtual int load ( FILE *fd ) ;
	/**
	**\brief Override ssgBranch methods.
	*/
	virtual int save ( FILE *fd ) ;
	/**
	**\brief Required when extending ssgBranch<br>
	*/
	virtual void copy_from ( chirp *src, int clone_flags ) ;

	ssgBranch *m_Xfm[2]; //this specifies the scene dependants.
	
private:
	GLubyte e[checkImageHeight][checkImageWidth][3]; /**This baby is on the display*/
	
	static int pixelWidth;  //=800;
	static int pixelHeight; //=800;
	static int vertP1;//=299; //value of verticle parralax in pixles. must be less than checkImageHeight
	static int vertP2;//=301; //value of verticle parralax in pixles. must be less than checkImageHeight
	static double alphaz;

	
	

};




#endif