#include "chirpPoly.h"

#define width  800
#define height 600

/**Draws a chirp to using polygons.
*x,y,z are the locations - w,h is the ratio of width to height - use 8,6
*/
chirpPoly::drawChirp(const float x, const float y,const float z,const float w,const float h)
{xloc=x;
	yloc=y;
	zloc=z;

	
	glBindTexture(GL_TEXTURE_2D, g_Texture[0]);

	glBegin(GL_QUADS);
	 glTexCoord2f(0.0f, 1.0f);
	 glVertex3f(x+w, y + h, z);					// Here is the top point of the triangle
	 glTexCoord2f(0.0f, 0.0f);
	 glVertex3f(x + w, y - h, z);			// Here is the right point of the triangle
	 glTexCoord2f(1.0f, 0.0f);
	 glVertex3f(x - w, y - h, z);			// Here is the left point of the triangle
	 glTexCoord2f(1.0f, 1.0f);
	 glVertex3f(x - w, y + h, z);			// Here is the left point of the triangle
	glEnd();											// This is the END of drawing


		//glDisable(GL_TEXTURE_2D); /**WHEN YOU WANT TO DRAW OPAIC PIXEL 

//	std::cout<<chirpPoly::numchirps<<",";
	/*
	glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1, 1, 0);

		// Display the bottom left vertice
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1, -1, 0);

		// Display the bottom right vertice
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1, -1, 0);

		// Display the top right vertice
		glTexCoord2f(1.0f, 1.0f);

  */
/*	glBegin (GL_TRIANGLES);								// This is our BEGIN to draw

		glColor3ub(255, 0, 0);							// Make the top vertex RED
		glVertex3f(x, y + height, z);					// Here is the top point of the triangle

		glColor3ub(0, 255, 0);							// Make the left vertex GREEN
		glVertex3f(x + width, y - height, z);			// Here is the right point of the triangle

		glColor3ub(0, 0, 255);							// Make the right vertex BLUE
		glVertex3f(x - width, y - height, z);			// Here is the left point of the triangle
	glEnd();	
*/	
/*	
	glBegin(GL_POLYGON);
		glVertex3f(xloc, yloc, zc);
		glVertex3f(xloc, yloc+yc, zc);
		glVertex3f(xloc+xc, yloc+yc, zc);
		glVertex3f(xloc+xc, yloc, zc);
		//glVertex3f(0.0, 0, 0.0);
	glEnd();
*/
}


/** 
at this stage the line will be a fixed length
This method will create a line of chirps.
Specifying the start and finish of the line.
**/
chirpPoly::drawLine(const float startx, const float starty, const float startz, const float finx, const float finy, const float finz)
{

	
	glBindTexture(GL_TEXTURE_2D, g_Texture[1]);

	glBegin(GL_QUADS);
	 glTexCoord2f(0.0f, 1.0f);
	 glVertex3f(startx, starty, startz);					// Here is the top point of the triangle
	 glTexCoord2f(0.0f, 0.0f);
	 glVertex3f(startx, finy, startz);			// Here is the right point of the triangle
	 glTexCoord2f(1.0f, 0.0f);
	 glVertex3f(finx, finy, finz);			// Here is the left point of the triangle
	 glTexCoord2f(1.0f, 1.0f);
	 glVertex3f(finx, starty, finz);			// Here is the left point of the triangle
	glEnd();




}




/** 
at this stage the line will be a fixed length
This method will create a line of chirps.
Each end of the polygon can be skewed to simulate depth HOPE THIS WORKS>>>>>
i.e. to make a deeper line at one end, just stretch the polygon at that end.
**/
void chirpPoly::drawLineSkew(const float startx, const float starty, const float startz, const float startWidth,const float finx, const float finy, const float finz,const float finWidth)
{

	
	glBindTexture(GL_TEXTURE_2D, g_Texture[1]);
	int nx=10,ny=10;
	glBegin(GL_TRIANGLE_STRIP);
//	for(int i=0;i<nx;i++)
	{
	//need to have more points on the polygon to allow smooth scaling
	//called subdivision. maybe can do it with a fan from the center of the polygon	
	 glTexCoord2f(0.0f, 1.0f);
	 glVertex3f(startx, starty, startz);					// Here is the top point of the triangle
	 glTexCoord2f(0.0f, 0.0f);
	 glVertex3f(startx, starty+startWidth, startz);			// Here is the right point of the triangle
	 glTexCoord2f(1.0f, 0.0f);
	 glVertex3f(finx, starty+finWidth/2, startz);			// Here is the left point of the triangle
	 glTexCoord2f(1.0f, 1.0f);
	 glVertex3f(finx, starty-finWidth/2, startz);			// Here is the left point of the triangle
	
	}
	 
	glEnd();




}








/**Sets the position of a chirp
**/
chirpPoly::setPos (const float x,const float y,const float z) 
{
	xloc=x;
	yloc=y;
	zloc=z;


}
/**Constructs a new polygon base chirp
*/
chirpPoly::chirpPoly (const float x,const float y,const float z,int n) 
{
//	CreateTexture(g_Texture, "chirp.bmp", 0);			// Load "bitmap.bmp" into openGL as a texture
	chirpPoly::numchirps=n;
	chirpPoly::fact=1.0f/(float)chirpPoly::numchirps;
	xloc=x;
	yloc=y;
	zloc=z;
CreateTexture(g_Texture, "chirpline.bmp", 1);			// Load "bitmap.bmp" into openGL as a texture
CreateTexture(g_Texture, "chirp.bmp", 0);			// Load "bitmap.bmp" into openGL as a texture
//unsigned int g_Texture2[MAX_TEXTURES];  // This will reference to our texture data stored with OpenGL UINT is an unsigned int (only positive numbers)
//unsigned int g_Texture[MAX_TEXTURES];  // This will reference to our texture data stored with OpenGL UINT is an unsigned int (only positive numbers)
	
/*
Create the primitives.
	point
	line
	polygon
  *
*/



	xloc=0;
	yloc=0;
	zloc=0;

	
	glBindTexture(GL_TEXTURE_2D, g_Texture[0]);
//	glTexEnv(), 
	chirpp = glGenLists(2);
	linep = chirpp+1;

	//The chirp primitive
	glNewList(chirpp, GL_COMPILE);
	glBindTexture(GL_TEXTURE_2D, g_Texture[0]);
	glPushMatrix();
	glTranslatef(-.62,-.5,0); //translate to ensure chirp is at centre (THIS IS DIFFERENT FOR EACH IMAGE
	glBegin(GL_QUADS);
	 glTexCoord2f(0.0f, 0.0f);
	 glVertex4f(0, 0, 0,(float)chirpPoly::numchirps);					// Here is the top point of the triangle
	 glTexCoord2f(1.0f, 0.0f);
	 glVertex4f(1.25, 0, 0,(float)chirpPoly::numchirps);			// Here is the right point of the triangle
	 glTexCoord2f(1.0f, 1.0f);
	 glVertex4f(1.25, 1, 0,(float)chirpPoly::numchirps);			// Here is the left point of the triangle
	 glTexCoord2f(0.0f, 1.0f);
	 glVertex4f(0,1, 0,(float)chirpPoly::numchirps);			// Here is the left point of the triangle
	glEnd();	  
//These are for the non-transparent bit in the center
//	glEnable(GL_DEPTH); 
//	glBegin(GL_QUADS);
//		glVertex4f(.5+.02,.5+.02,0,1);
//		glVertex4f(.5+.02,.5-.02,0,1);
//		glVertex4f(.5-.02,.5-.02,0,1);
//		glVertex4f(.5-.02,.5+.02,0,1);
//	glEnd();
//	glDisable(GL_DEPTH);
	glPopMatrix();
	
	glEndList();
	
	//the line primitive
	glNewList(linep, GL_COMPILE);
	 drawChirpC(-.2,0,0);
	 drawChirpC(.2,0,0);
	 for(int ii=1;ii<numchirps-1;ii++)
	 {	float inc=(-.2f+.4f*ii/(float)numchirps);  /**TODO need to have a linedetail number.*/
 		drawChirpC(inc,0,.6); //z=.6 is not bad
	 }
	
	
	glEndList();
	

//	glNewList(point,GL_COMPILE);
	//	.../* GL Commands to model */
//	glEndList();


}

/**draws a compiled line primitive*/
void chirpPoly::drawLineC(const float x, const float y,const float z,float rot)
{//this version just rotates the line about x y and z
	glPushMatrix();
	glRotatef(rot,0,1,0);
	glTranslatef(-x, -y, z);
	glCallList(linep);
	glPopMatrix();
	
	
}


/**Draws a compiled chirp should be faster*/
void chirpPoly::drawChirpC(const float x, const float y,const float z)
{
	glPushMatrix();
	glTranslatef(-x, -y, z);
	glCallList(chirpp);
	glPopMatrix();
	


}

/** Creates a texture.
*From the openGL website
*/
void chirpPoly::CreateTexture(UINT textureArray[], LPSTR strFileName, int textureID)
{
	AUX_RGBImageRec *pBitmap = NULL;
	
	if(!strFileName)									// Return from the function if no file name was passed in
		return;
	
	// We need to load the texture data, so we use a cool API that the glaux.lib offers.
	
	pBitmap = auxDIBImageLoad(strFileName);				// Load the bitmap and store the data
	
	if(pBitmap == NULL)									// If we can't load the file, quit!
		exit(0);

	// Now that we have the texture data, we need to register our texture with OpenGL
	// To do this we need to call glGenTextures().  The 1 for the first parameter is
	// how many texture we want to register this time (we could do a bunch in a row).
	// The second parameter is the array index that will hold the reference to this texture.

	// Generate a texture with the associative texture ID stored in the array
	glGenTextures(1, &textureArray[textureID]);

	// Now that we have a reference for the texture, we need to bind the texture
	// to tell OpenGL this is the reference that we are assigning the bitmap data too.
	// The first parameter tells OpenGL we want are using a 2D texture, while the
	// second parameter passes in the reference we are going to assign the texture too.
	// We will use this function later to tell OpenGL we want to use this texture to texture map.

	// Bind the texture to the texture arrays index and init the texture
	glBindTexture(GL_TEXTURE_2D, textureArray[textureID]);

	// Now comes the important part, we actually pass in all the data from the bitmap to
	// create the texture. Here is what the parameters mean in gluBuild2DMipmaps():
	// (We want a 2D texture, 3 channels (RGB), bitmap width, bitmap height, It's an RGB format,
	//  the data is stored as unsigned bytes, and the actuall pixel data);

	// What is a Mip map?  Mip maps are a bunch of scaled pictures from the original.  This makes
	// it look better when we are near and farther away from the texture map.  It chooses the
	// best looking scaled size depending on where the camera is according to the texture map.
	// Otherwise, if we didn't use mip maps, it would scale the original UP and down which would
	// look not so good when we got far away or up close, it would look pixelated.

	// Build Mipmaps (builds different versions of the picture for distances - looks better)
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, pBitmap->sizeX, pBitmap->sizeY, GL_RGB, GL_UNSIGNED_BYTE, pBitmap->data);

	// Lastly, we need to tell OpenGL the quality of our texture map.  GL_LINEAR_MIPMAP_LINEAR
	// is the smoothest.  GL_LINEAR_MIPMAP_NEAREST is faster than GL_LINEAR_MIPMAP_LINEAR, 
	// but looks blochy and pixilated.  Good for slower computers though.  Read more about 
	// the MIN and MAG filters at the bottom of main.cpp
		
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);

	// Now we need to free the bitmap data that we loaded since openGL stored it as a texture

	if (pBitmap)										// If we loaded the bitmap
	{
		if (pBitmap->data)								// If there is texture data
		{
			free(pBitmap->data);						// Free the texture data, we don't need it anymore
		}

		free(pBitmap);									// Free the bitmap structure
	}
}


