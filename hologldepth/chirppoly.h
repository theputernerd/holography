#ifndef _chirpPoly_H_
#define _chirpPoly_H_
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "glaux.lib")
#include <GL/glut.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include <GL\glaux.h>


#define MAX_TEXTURES 3
/**
*This class creates and manipulates chirps using polygons.
*
*/
class chirpPoly
{
public:

	
	chirpPoly::chirpPoly (const float x,const float y,const float z,int n);
	drawChirp(const float x, const float y,const float z,const float w,const float h);
	drawLine(const float startx, const float starty, const float startz, const float finx, const float finy, const float finz);
	setPos(const float x,const float y,const float z) ;
	int numchirps;
	float fact;
	unsigned int g_Texture[MAX_TEXTURES];  // This will reference to our texture data stored with OpenGL UINT is an unsigned int (only positive numbers)
	void drawChirpC(const float x, const float y,const float z);
	void chirpPoly::drawLineC(const float x, const float y,const float z,float rot);
	void chirpPoly::drawLineSkew(const float startx, const float starty, const float startz, const float startWidth,const float finx, const float finy, const float finz,const float finWidth);

private:
	float xloc,yloc,zloc;
	static void CreateTexture(UINT textureArray[], LPSTR strFileName, int textureID);
	GLuint chirpp,linep; // this is the stored object reference number

};




#endif