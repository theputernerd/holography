#include <time.h>
#include <iostream>
#include "chirpPoly.h"
#include <GL\glut.h>

#define width  800
#define height 600
#define PI 3.142
#define FRAME_RATE_SAMPLES 50

#include <time.h>
#include <iostream>
#include "chirpPoly.h"
#include <GL\glut.h>

float xloc=6.8,yloc=1.4,zloc=1.2;
float xloc2=6.6,yloc2=1.4,zloc2=1.2;
double speed=.0;
bool reverse=true;
char *title;//=new;
int nseconds=0;
int nframes=0;
int minfps=10000,maxfps=0;
double counter=0;
int nchirps=2; //can do 400 chirps and still have 2 frames per sec with compiled line.
chirpPoly* cp;
float offset=0; //This is used to create off axis images - use addition of this in the x term of any demos
int FrameCount=0;
float FrameRate=0;
float contrast=0;


/**Courtsey of OpenGL.org. This function will keep track of the frame rates
*/
static void ourDoFPS() 
{
   static clock_t last=0;
   clock_t now;
   float delta;

   if (++FrameCount >= FRAME_RATE_SAMPLES) {
      now  = clock();
      delta= (now - last) / (float) CLOCKS_PER_SEC;
      last = now;

      FrameRate = FRAME_RATE_SAMPLES / delta;
      FrameCount = 0;
   }
}

/**Courtsey of OpenGL.org. This function will keep track of the fram rates
*/
void CalculateFrameRate()
{
	// Below we create a bunch of static variables because we want to keep the information
	// in these variables after the function quits.  We could make these global but that would
	// be somewhat messy and superfluous.  Note, that normally you don't want to display this to
	// the window title bar.  This is because it's slow and doesn't work in full screen.
	// Try using the 3D/2D font's.  You can check out the tutorials at www.gametutorials.com.

	static float framesPerSecond    = 0.0f;							// This will store our fps
    static float lastTime			= 0.0f;							// This will hold the time from the last frame
	static char strFrameRate[50] = {0};								// We will store the string here for the window title

	// Here we get the current tick count and multiply it by 0.001 to convert it from milliseconds to seconds.
	// GetTickCount() returns milliseconds (1000 ms = 1 second) so we want something more intuitive to work with.
    float currentTime = GetTickCount() * 0.001f;				

	// Increase the frame counter
    ++framesPerSecond;

	// Now we want to subtract the current time by the last time that was stored.  If it is greater than 1
	// that means a second has passed and we need to display the new frame rate.  Of course, the first time
	// will always be greater than 1 because lastTime = 0.  The first second will NOT be true, but the remaining
	// ones will.  The 1.0 represents 1 second.  Let's say we got 12031 (12.031) from GetTickCount for the currentTime,
	// and the lastTime had 11230 (11.230).  Well, 12.031 - 11.230 = 0.801, which is NOT a full second.  So we try again
	// the next frame.  Once the currentTime - lastTime comes out to be greater than a second (> 1), we calculate the
	// frames for this last second.
    if( currentTime - lastTime > 1.0f )
    {	nseconds++;

		// Here we set the lastTime to the currentTime.  This will be used as the starting point for the next second.
		// This is because GetTickCount() counts up, so we need to create a delta that subtract the current time from.
	    lastTime = currentTime;
		nframes+=framesPerSecond;
		// Copy the frames per second into a string to display in the window title bar
		//sprintf(strFrameRate, "Current Frames Per Second: %d", int(framesPerSecond));
		if(framesPerSecond<minfps) minfps=framesPerSecond;
		if(framesPerSecond>maxfps) maxfps=framesPerSecond;

		// Set the window title bar to our string
		//SetWindowText(hndl, strFrameRate);
		//title=strFrameRate;
		// Reset the frames per second
        framesPerSecond = 0;
		}
}

/**
**\brief Called by opengl on redraw.
*
*The first thing it does is calls the user supplied function
*displayGUI(); then marks the window for redrawing.
*/	
void display()
{	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
	glLoadIdentity();									// Reset The matrix
	
		// 	  Position      View	   Up Vector
	gluLookAt(0, 0, 0,     0, 0, 6,     0, 1, 0);		// This determines where the camera's position and view is


	//glDisable(GL_DEPTH);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT  );
   
	glEnable(GL_BLEND);
	counter+=speed;

	//SO MANY COMMENTS HERE BECAUSE THESE ARE DIFFERENT OBJECTS DISPLAYED
	//

	// 3 axes by themselves
//	cp->drawChirp( xloc*sin(counter)+offset, 0, 7, 8, 6);
//	cp->drawChirp( 0+offset, yloc*sin(counter), 7, 8, 6);
//	cp->drawChirp( 0+offset, 0, zloc*sin(counter)+7.5, 8, 6);
			

  //cp->drawChirp(xloc,yloc,zloc,8,6);
	/*Electrons moving around an atom, circular orbit*/
//	cp->drawChirp( xloc*sin(counter)+offset, xloc*cos(counter), 3, 8, 6);
//	cp->drawChirp( 0+offset, yloc*sin(counter), yloc*cos(counter)+3.5, 8, 6);
//	cp->drawChirp( zloc*sin(counter)+offset, 0, zloc*cos(counter)+3.5, 8, 6);
//	cp->drawChirp( xloc2*sin(counter)+offset, yloc2*sin(counter), zloc2*cos(counter)+3.5, 8, 6);

	//cp->drawLine(xloc,.084+yloc,zloc,.34+xloc2,2.6+yloc2,zloc2);

	/**A line of chirps across the screen*/
//	for(int ii=0;ii<=nchirps-1;ii++)
	{
//		cp->drawChirp((-2+4*ii/(nchirps-1)),0,5,8,6);
//		cp->drawChirp(-2,0,5,8,6);
	
	
	}
	


//	for(float ii=0;ii<=nchirps/2.0f;ii++)
	{
		//cp->drawChirp(ii,0,5,8,6); //this one for normal line
		
//		cp->drawChirp(xloc+(xloc2-xloc)*ii/nchirps/2.0f+offset,yloc+(yloc2-yloc)*ii/nchirps/2.0f,zloc+(zloc2-zloc)*ii/nchirps/2.0f,8,6);
	    
//		cp->drawChirp(2*xloc+(yloc2-2*xloc)*ii/nchirps/2.0f+offset,yloc+(yloc2-xloc)*ii/nchirps/2.0f,zloc+(zloc2-zloc)*ii/nchirps/2.0f,8,6);
		
		//std::cout<<ii<<",";
//		cp->drawChirp(2,0,5,8,6);
	
	}


	//This one is the supafast one 100chirps and still get 6 frames/sec
/*	for(int ii=0;ii<nchirps;ii++)
	{	float inc=(-.2+.4*ii/nchirps);
		cp->drawChirpC(inc,0,.6);
		//std::cout<<ii<<",";
		//cp->drawChirp(2,0,5,8,6);
	
	}
*/
//	cp->drawChirp(-.42+.34f+xloc,2.2+yloc,2.44+zloc,8,6); 
/*This one creates a circle singlechirpcircle*/
//	cp->drawChirp(-.42f+(.74f)*cos(counter)+xloc,
//		 1.273+1.189*sin(counter)+yloc,2.5 +2 *cos(counter),8,6);
//	cp->drawChirp(-.42f+(.74f)*sin(counter)+xloc2,
//		 1.273+1.189*cos(counter)+yloc2,2.5 +2*sin(counter)+zloc2,8,6);

//raster a line 	
//	cp->drawChirp(-.42f+(.74f)*cos(counter)+xloc,
//		 1.273+yloc,2.44+(.5f)+zloc,8,6);
//	cp->drawChirp(-.42f+xloc,
//		 1.273+1.189*sin(counter)+yloc,2.44+zloc,8,6);

	cp->drawChirp(2,0,5,8,6);
	cp->drawChirp(-2,0,5,8,6);
	//
	//cp->drawChirp(-.42+.34f+xloc,2.2+yloc,2.44+zloc,8,6);
	//cp->drawChirp(-.42+.34f+xloc,2.2+yloc,2.44+zloc2,8,6);
	
//	cp->drawChirp(-.42f+(.74f)*cos(counter)+xloc,1.273+yloc/2.0f,2.44+zloc,8,6);
//	cp->drawLine(xloc,yloc,zloc,xloc+.42f,yloc+1.27,2.44+zloc);


//	cp->drawLineC(xloc,yloc,zloc,xloc2);

//	cp->drawLine(-70,-48,115,30,-48+100,36.5);
//	cp->drawLine(60,50,108.5,60-100,50-100,60);
//	cp->drawLine(60+xloc,50+yloc,60+zloc,60-100+xloc,50-100+yloc,108.5+zloc);
	//cp->drawLine(xloc,yloc,zloc,xloc-100,yloc-100,zloc2);

	float currentTime = GetTickCount() * 0.001f;
//	cp->drawChirp(1,0,5,8,6);
//	cp->drawChirp(1,0,xloc,8,6);
	
/*	float l=xloc2;
	cp->drawChirp(xloc-l,yloc-l,zloc-l,8,6);
	cp->drawChirp(xloc+l,yloc-l,zloc-l,8,6);
	cp->drawChirp(xloc+l,yloc+l,zloc-l,8,6);
	cp->drawChirp(xloc-l,yloc+l,zloc-l,8,6);
	cp->drawChirp(xloc-l,yloc-l,zloc+l,8,6);
	cp->drawChirp(xloc+l,yloc-l,zloc+l,8,6);
	cp->drawChirp(xloc+l,yloc+l,zloc+l,8,6);
	cp->drawChirp(xloc-l,yloc+l,zloc+l,8,6);
*/	
		//	cp->drawChirp(.1,0,5,8,6);
//	cp->drawChirp(.2,0,5,8,6);
//	cp->drawChirp(.3,0,5,8,6);
//	cp->drawChirp(.4,0,5,8,6);
	
	
	
	
	
	/**LArge number ofpoints 100or so. line getting further away l to right.*/
/*	for(int i=0;i<nchirps;i++)
	{
		//cp->drawChirp(-1,0,14, 8, 6);
		cp->drawChirp(4*i/nchirps-2,0,7*i/nchirps+.5, 8, 6);
		

	}
*/
	//	cp->drawChirp( 1, 0, 8, 1, 1);
//	cp->drawChirpC(xloc,yloc,zloc);
	//	cp->drawChirp( -1, 0, 8, 1, 1);						// Draw the triangle at the origin (But will appear back and to the left)
	glDisable(GL_BLEND);

		glutSwapBuffers();
	CalculateFrameRate();
	if(counter>8*PI) counter=0;

}
/**Callback for keyboard events
*/
static void keyboard ( unsigned char key, int, int )
{
	switch (key)
	{	case 'a':
			xloc+=.2;
	    break;
		case 's':
			yloc+=.2;
	    break;
	
		case 'd':
			zloc+=.05;
	    break;
		case 'z':
			xloc-=.2;
	    break;
		case 'x':
			yloc-=.2;
	    break;
		case 'c':
			zloc-=.2;
	    break;
		
		case 'r':
			printf("redrawing and resetting\n");
			nframes=0;
			nseconds=0;
			minfps=10000;
			maxfps=0;
			
			glutPostRedisplay();
	    break;
		case 'i':
			if(nseconds>0)
			std::cout<<"("<<xloc<<","<<yloc<<","<<zloc<<")";
			std::cout<<"("<<xloc2<<","<<yloc2<<","<<zloc2<<")\n";
			std::cout<<"speed "<<speed<<"\n";
			std::cout<<"offset "<<offset<<"\n";
			std::cout<<"nchirps"<<nchirps<<"\n";
			std::cout<<"contrast"<<contrast<<"\n";
			std::cout<<"fpsRange "<<minfps<<":"<<maxfps<<"\n";
			if(nseconds>0)
			  std::cout<<"Ave fps:"<<(float)(nframes/nseconds)<<"\n";
			
	
		break;
		case 'j':
			xloc2+=.2;
	    break;
		case 'k':
			yloc2+=.2;
	    break;
	
		case 'l':
			zloc2+=.005;
	    break;
		case 'm':
			xloc2-=.2;
	    break;
		case ',':
			yloc2-=.2;
	    break;
		case '.':
			zloc2-=.2;
	    break;
		case '=':
			speed+=.005;
		break;
		case '-':
			speed-=.005;
			if(speed<0) speed=0;
		break;
		case 'o':
			offset+=.2;
		break;
		case 'p':
			offset-=.2;
		break;

		case '2':
			nchirps+=1;
			cp->numchirps+=1;
			glColor4d(1.0f,1.0f,1.0f,1.0f/(GLfloat)(nchirps+contrast));
		break;
		case '1':
			nchirps-=1;
			cp->numchirps-=1;
			glColor4d(1.0f,1.0f,1.0f,1.0f/(GLfloat)(nchirps+contrast));
		break;
		case '7':
			contrast-=5;
			glColor4d(1.0f,1.0f,1.0f,1.0f/(GLfloat)(nchirps+contrast));
		break;
		case '6':
			contrast+=5;
			glColor4d(1.0f,1.0f,1.0f,1.0f/(GLfloat)(nchirps+contrast));
		break;
	
	}
	cp->setPos(xloc,yloc,zloc);

}

/*
  The GLUT window reshape event
*/
static void reshape ( int w, int h )
{  


	glViewport(0,0,w,h);						// Make our viewport the whole window
														// We could make the view smaller inside
														// Our window if we wanted too.
														// The glViewport takes (x, y, width, height)
														// This basically means, what our our drawing boundries

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

														// Calculate The Aspect Ratio Of The Window
														// The parameters are:
														// (view angle, aspect ration of the width to the height, 
														//  The closest distance to the camera before it clips, 
				  // FOV		// Ratio				//  The farthest distance before it stops drawing)
	gluPerspective(45.0f,(GLfloat)w/(GLfloat)h, .005f ,150.0f);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();
	

   glutPostRedisplay();
   nframes=0;
   nseconds=0;
   minfps=10000;
   maxfps=0;

  		 
}



/*
*Initialise GLUT and whatever else
*/
void initialise()
{	int   fake_argc = 1 ;
	char *fake_argv[3] ;
	fake_argv[0] = "Holography Image" ;
	fake_argv[1] = "HoloGL Version 3" ;
	title="Version 1";
	fake_argv[2] = NULL ;
	glutInit               ( &fake_argc, fake_argv ) ;
 	glutInitDisplayMode    ( GLUT_RGBA | GLUT_DOUBLE ) ;
  	glutInitWindowPosition ( 0, 0 ) ;
	glutInitWindowSize     ( 800, 600 ) ;
   	
	glutCreateWindow       ( fake_argv[1] ) ;
	
	glutIdleFunc(display);
	glutDisplayFunc        ( &display   ) ;
	glutReshapeFunc        ( &reshape  ) ;
	glutKeyboardFunc       ( &keyboard ) ;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT  );
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height, .005f ,150.0f);
	//gwindow->hWnd
	glViewport( 0, 0, 800, 600);
	//PIXELFORMATDESCRIPTOR;
	//gluPerspective(0,0,0,100);
	glClearDepth(1.0);
	glDepthFunc(GL_LESS); 
	//glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_DST_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glColor4f(1.0f,1.0f,1.0f,1.0f/nchirps);		// Full Brightness, 50% Alpha ( NEW )
	glBlendFunc(GL_SRC_ALPHA,GL_DST_ALPHA);	// Blending Function For Translucency Based On Source Alpha Value


}
/**
*The main method
*/
int main(int argc, char* argv[])
{
//	chirp* c1=new chirp;
	initialise();
	//this Chamber object creates a subwindow in mainWindow
	//d=new Chamber(20,0,mainWindow,400,0,800,600); //multiple instances doesn't work fully

	cp=new chirpPoly(xloc,yloc,zloc,nchirps);
	std::cout<<MAX_TEXTURES<<"\n";
	glutMainLoop();
	
	
	return 0;
}